﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniqueElement
{
    class Program
    {
        static void Main(string[] args)
        {
            //Test code
            int[] inputArray = { 1, 4, 2, 2, 3, 3, 1 };
            Console.Write(getUniqueElement(inputArray));
            Console.ReadKey();
        }

        static int getUniqueElement(int[] inputArray)
        {
            //Start with the first element
            int current = inputArray[0];

            //Loop through all the elements
            //run exclusive OR for all elements
            //this will leave the only unique number
            for (int i = 1; i < inputArray.Length; i++)
                current = current ^ inputArray[i];

            return current;
        }
    }
}

